if (canvas.tokens.controlled.length === 0) ui.notifications.warn("Please select one or more tokens first.");
else {
  async function rollInit() {
    const rollMode = game.settings.get("core", "rollMode");
    if (event.shiftKey) {
      for (let token of canvas.tokens.placeables) {      
        if (token.inCombat === false) await token.toggleCombat().then(() => game.combat.rollNPC({rollMode: rollMode}));
        else await game.combat.rollNPC({rollMode: rollMode});
      }
    }
    else {
      for (let token of canvas.tokens.controlled) {      
        if (token.inCombat === false) await token.toggleCombat().then(() => game.combat.rollNPC({rollMode: rollMode}));
        else await game.combat.rollNPC({rollMode: rollMode});
      }
    }
  }
  rollInit();
}
