async function promptInit() {
  let updates = [];
  for (let thisCombatant of canvas.tokens.controlled) {
    let [newInit] = await new Promise((resolve, reject) => {
      const content = `<div class="form-group dialog distance-prompt">
                         <label>${thisCombatant.name}'s Initiative:</label> 
                         <input id="init" type="number" name="init" value="" />
                       </div>`;
      setTimeout(function() {
          new Dialog({
          title: "Update Initiative",
          content: content,
          default: 'ok',
          buttons: {
            ok: {
              icon: '<i class="fas fa-check"></i>',
              label: 'Done',
              default: true,
              callback: async (html) => {
                const newinit = html.find('.distance-prompt.dialog [id="init"]')[0].value;
                if (thisCombatant.inCombat == false) await thisCombatant.toggleCombat().then(() => resolve([{_id: game.combat.getCombatantByToken(thisCombatant.data._id)._id, initiative: newinit}]));
                else await resolve([{_id: game.combat.getCombatantByToken(thisCombatant.data._id)._id, initiative: newinit}]);
                await game.combat.updateCombatant(updates);
              },
            }
          }
        }).render(true);
        setTimeout(function() {
          document.getElementById("init").value = (thisCombatant.inCombat == true && Number(game.combat.getCombatantByToken(thisCombatant.data._id)?.initiative)) ? game.combat.getCombatantByToken(thisCombatant.data._id).initiative : null;
          document.getElementById("init").focus();
        }, 0);
      },200);
    });
    updates.push(newInit);
  }
}
promptInit();
