// Create an extra weapon item in your world that does the spell's damage, put this on the spell, and it will create the item on the actor and update its data.
// DAE Usage: Macro => Execute "Minute Meteors Macro" @token @spellLevel @attributes.spelldc
const lastArg = args[args.length-1];
let cToken = canvas.tokens.get(args[1]);
let cActor = (cToken) ? cToken.actor : game.actors.get(lastArg.actorId);

if (args[0] === "on") {
	const filterItem = game.items.filter(i => i.type === "weapon" && i.name === "Melf's Minute Meteors");
	if (filterItem) {
		for (let i = 0; i < filterItem.length; i++) {
			let charges = 4 + ((parseInt(args[2]) - 2) * 2);
			let saveDC = parseInt(args[3]);
			let base_item = filterItem[i];
			let copy_item = duplicate(base_item);
			copy_item.data.uses.value = charges;
			copy_item.data.uses.max = charges;
			copy_item.data.save.dc = saveDC;
			copy_item.data.proficient = true;
			copy_item.data.equipped = true;
			cActor.createOwnedItem(copy_item);
		}
	}
}

if (args[0] === "off") {
	await cActor.deleteOwnedItem(cActor.items.filter(i => i.type === "weapon" && i.name === "Melf's Minute Meteors").map(i => i._id));
}
