// Darkness Macro
// Applies a darkness lighting animation to a target creature.
// Usage: Macro => Execute "Darkness Macro" @token
const lastArg = args[args.length-1];
let target = canvas.tokens.get(lastArg.tokenId);
let cToken = canvas.tokens.get(args[1]);

if (args[0] === "on") {
	DAE.setFlag(target, "darkness", {"brightLight": target.data.brightLight, "lightAnimation": target.data.lightAnimation});
	target.update({"brightLight": -15, "lightAnimation": {"speed": 1, "intensity": 1, "type": "roiling"}});
}

if (args[0] === "off") {
	let flag = await DAE.getFlag(target, "darkness");
	await target.update({"brightLight": flag.brightLight, "lightAnimation": flag.lightAnimation});
	DAE.unsetFlag(target, "darkness");
	if (cToken && game.cub.hasCondition("Concentrating", cToken, {warn: false})) await game.cub.removeCondition("Concentrating", cToken, {warn: false});
}
