// Motivational Speech Macro
// Usage: Macro => Execute "Motivational Speech Macro" @spellLevel
const lastArg = args[args.length-1];
let spellBonus = 5 * (parseInt(args[1]) - 2);
let tactor = (lastArg.tokenId) ? canvas.tokens.get(lastArg.tokenId).actor : game.actors.get(lastArg.actorId);
let tempHp;
if (tactor) tempHp = (Number(tactor.data.data.attributes.hp.temp) || 0);

if (args[0] === "on" && tempHp < spellBonus) {
  tactor.update({"data.attributes.hp.temp": spellBonus});
}

if (args[0] === "each" && tempHp === 0) {
  await tactor.deleteEmbeddedEntity("ActiveEffect", lastArg.effectId);
}

if (args[0] === "off" && tempHp !== null && (tempHp <= spellBonus)) {
  tactor.update({"data.attributes.hp.temp": null});
}
