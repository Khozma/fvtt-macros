// Prismatic Spray | MidiQOL OnUse Macro
// Put on a spell with type Saving Throw and no damage.
// To-do: automate the Indigo/violet rays' save on each turn effect rather than just apply the condition.
if (args[0]?.tag === "OnUse" && (args[0].targets.length > 0)) {
	const hitTargets = args[0].hitTargets;
	const failSaveIds = args[0].failedSaves.map(a => a.actorId);
	let cToken = canvas.tokens.get(args[0].tokenId);
	let cActor = (cToken) ? cToken.actor : game.actors.get(args[0].actor._id);
	let Cub_Condition = game.macros.getName("Cub_Condition");

	let numDice = 10;
	let damageRoll = new Roll(`${numDice}d6`).roll();
	if (game.modules.get("dice-so-nice")?.active) game.dice3d.showForRoll(damageRoll);

	async function wait(ms) {
	  return new Promise(resolve => {
	    setTimeout(resolve, ms);
	  });
	}

	async function colorRay(actor, token, target, damageRoll, colorRoll) {
		let rollMode = game.settings.get("core", "rollMode");
		let damageFlavor = "Color Ray - Damage Roll";
		let damageTotal = damageRoll.total;
		let failed = (failSaveIds || "").includes(target.data.actorId);
		if (!failed) damageTotal = Math.floor(damageTotal * 0.5);

		switch (colorRoll.total) {
			case 1:
				colorRoll.toMessage({flavor: "Color Ray - Red (Fire)", rollMode, user: game.user._id, speaker: ChatMessage.getSpeaker({token: target})})
				new MidiQOL.DamageOnlyWorkflow(actor, token, damageTotal, "fire", [target], damageRoll, {flavor: damageFlavor, itemData: args[0].item, itemCardId: args[0].itemCardId, useOther: true})
				break;
			case 2:
				colorRoll.toMessage({flavor: "Color Ray - Orange (Acid)", rollMode, user: game.user._id, speaker: ChatMessage.getSpeaker({token: target})})
				new MidiQOL.DamageOnlyWorkflow(actor, token, damageTotal, "acid", [target], damageRoll, {flavor: damageFlavor, itemData: args[0].item, itemCardId: args[0].itemCardId, useOther: true})
				break;
			case 3:
				colorRoll.toMessage({flavor: "Color Ray - Yellow (Lightning)", rollMode, user: game.user._id, speaker: ChatMessage.getSpeaker({token: target})})
				new MidiQOL.DamageOnlyWorkflow(actor, token, damageTotal, "lightning", [target], damageRoll, {flavor: damageFlavor, itemData: args[0].item, itemCardId: args[0].itemCardId, useOther: true})
				break;
			case 4:
				colorRoll.toMessage({flavor: "Color Ray - Green (Poison)", rollMode, user: game.user._id, speaker: ChatMessage.getSpeaker({token: target})})
				new MidiQOL.DamageOnlyWorkflow(actor, token, damageTotal, "poison", [target], damageRoll, {flavor: damageFlavor, itemData: args[0].item, itemCardId: args[0].itemCardId, useOther: true})
				break;
			case 5:
				colorRoll.toMessage({flavor: "Color Ray - Blue (Cold)", rollMode, user: game.user._id, speaker: ChatMessage.getSpeaker({token: target})})
				new MidiQOL.DamageOnlyWorkflow(actor, token, damageTotal, "cold", [target], damageRoll, {flavor: damageFlavor, itemData: args[0].item, itemCardId: args[0].itemCardId, useOther: true})
				break;
			case 6:
				colorRoll.toMessage({flavor: "Color Ray - Indigo (Restrained)", rollMode, user: game.user._id, speaker: ChatMessage.getSpeaker({token: target})})
				if (failed) await Cub_Condition.execute(target.id, "Restrained", "add");
				break;
			case 7:
				colorRoll.toMessage({flavor: "Color Ray - Violet (Blinded)", rollMode, user: game.user._id, speaker: ChatMessage.getSpeaker({token: target})})
				if (failed) await Cub_Condition.execute(target.id, "Blinded", "add");
				break;
			case 8:
				let rerollA = new Roll(`1d8r8`).roll();
				let rerollB = new Roll(`1d8r8`).roll();
				await colorRay(actor, token, target, damageRoll, rerollA);
				await colorRay(actor, token, target, damageRoll, rerollB);
				break;
		}
	}

	for (let hit of hitTargets) {
		let target = canvas.tokens.get(hit._id);
		let colorRoll = new Roll(`1d8`).roll();
		await colorRay(cActor, cToken, target, damageRoll, colorRoll);
	}

  await wait(1000);
  const chatMessage = await game.messages.get(args[0].itemCardId);
  let content = await duplicate(chatMessage.data.content);
	const searchString = /<div class="midi-qol-hits-display">[\s\S]*<div class="end-midi-qol-hits-display">/g;
	const replaceString = `<div class="midi-qol-hits-display"><div class="end-midi-qol-hits-display">`;
	content = await content.replace(searchString, replaceString);
  await chatMessage.update({content});
}
