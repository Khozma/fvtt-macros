// Crown of Stars Macro
// Create a world item with type Weapon called "Crown of Stars" that does the spell's damage, then this macro will add/remove it.
// Usage: Macro => Execute "Crown of Stars Macro" @token @spellLevel
const lastArg = args[args.length-1];
let cToken = canvas.tokens.get(args[1]);
let cActor = (cToken) ? cToken.actor : game.actors.get(lastArg.actorId);
const item = lastArg.efData.flags.dae.itemData;
let itemName = item.name;
const ability = (item.data.ability !== "") ? item.data.ability : cActor.data.data.attributes.spellcasting;

if (args[0] === "on") {
	const filterItem = game.items.filter(i => i.type === "weapon" && i.name === "Crown of Stars");
	if (filterItem?.length > 0) {
		let copyItem = duplicate(filterItem[0]);
		let spellLevel = parseInt(args[2]);
		let uses = 7;
		if (spellLevel > item.data.level) uses += 2 * (spellLevel - item.data.level);
		copyItem.name = itemName;
		copyItem.data.uses.value = uses;
		copyItem.data.uses.max = uses;
		copyItem.data.ability = ability;
		copyItem.data.equipped = true;
		copyItem.data.proficient = true;
		cActor.createOwnedItem(copyItem);
	}
}

if (args[0] === "off") {
	if (cActor.items?.filter(i => i.type === "weapon" && i.name === itemName)?.length > 0) await cActor.deleteOwnedItem(cActor.items.filter(i => i.type === "weapon" && i.name === itemName).map(i => i._id));
}
