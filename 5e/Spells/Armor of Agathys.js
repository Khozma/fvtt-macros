// Armor of Agathys Macro
// Usage: Macro => Execute "Armor of Agathys Macro" @token @spellLevel
// Set to macro repeat start of each turn.
const lastArg = args[args.length-1];
let item = lastArg.efData.flags.dae.itemData;
let cActor = (canvas.tokens.get(args[1])) ? canvas.tokens.get(args[1]).actor : game.actors.get(lastArg.actorId);
let tempHp;
if (cActor) tempHp = (Number(cActor.data.data.attributes.hp.temp) || 0);
let spellBonus = 5 * parseInt(args[2]);

if (args[0] === "on") {
  if (tempHp < spellBonus) cActor.update({"data.attributes.hp.temp": spellBonus});
  await cActor.createOwnedItem({
    "name": item.name,
    "type": "weapon",
    "data": {
      "description": {
        "value": item.data.description.value,
        "chat": "",
        "unidentified": ""
      },
      "activation": {
        "type": "special",
        "cost": null,
        "condition": "When a creature hits you with a melee attack"
      },
      "ability": "",
      "actionType": "other",
      "attackBonus": 0,
      "chatFlavor": "",
      "critical": null,
      "damage": {
        "parts": [
          [
            `${spellBonus}`,
            "cold"
          ]
        ],
        "versatile": ""
      },
      "formula": "",
      "save": {
        "ability": "",
        "dc": null,
        "scaling": "spell"
      },
      "armor": {
        "value": 10
      },
      "hp": {
        "value": 0,
        "max": 0,
        "dt": null,
        "conditions": ""
      },
      "weaponType": "natural",
      "equipped": true,
      "proficient": true,
      "identified": true,
      "properties": {
        "ada": false,
        "amm": false,
        "fin": false,
        "fir": false,
        "foc": false,
        "hvy": false,
        "lgt": false,
        "lod": false,
        "mgc": true,
        "rch": false,
        "rel": false,
        "ret": false,
        "sil": false,
        "spc": false,
        "thr": false,
        "two": false,
        "ver": false,
        "nodam": false,
        "fulldam": false
      },
    },
    "img": item.img,
    "effects": []
  });
}

if (args[0] === "each" && tempHp === 0) {
  await cActor.deleteEmbeddedEntity("ActiveEffect", lastArg.effectId);
}

if (args[0] === "off") {
  if (tempHp > 0 && tempHp <= spellBonus) cActor.update({"data.attributes.hp.temp": null});
  await cActor.deleteOwnedItem(cActor.items.filter(i => i.type === "weapon" && i.name === item.name).map(i => i._id));
}
