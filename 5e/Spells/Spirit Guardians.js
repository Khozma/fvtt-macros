// Spirit Guardians Macro | Requires DAE/TokenMagic/Active Auras
// Usage: Macro => Execute "Spirit Guardians Macro" @token @attributes.spellcasting @spellLevel "radiant"
// Triggers a save on creatures' turns, then deals full or half damage as appropriate. Configure as radiant/necrotic in the macro.execute args.
// Put on a spell with target Self. Set aura effect to 15 ft radius, check apply on turn and only once per turn. Set macro repeat to start of each turn.
const lastArg = args[args.length-1];
let target = canvas.tokens.get(lastArg.tokenId);
const cTokenId = args[1];
let cToken = canvas.tokens.get(cTokenId);
let isCaster = (target.id === cTokenId);
const damageType = args[4].toLowerCase();

function getSaveDC(ability, actor) {
  if (ability === "str") return actor.data.data.abilities.str.dc;
  else if (ability === "dex") return actor.data.data.abilities.dex.dc;
  else if (ability === "con") return actor.data.data.abilities.con.dc;
  else if (ability === "int") return actor.data.data.abilities.int.dc;
  else if (ability === "wis") return actor.data.data.abilities.wis.dc;
  else if (ability === "cha") return actor.data.data.abilities.cha.dc;
  else return actor.data.data.attributes.spelldc;
}
let ability = (lastArg.efData.flags.dae.itemData.data.ability === "") ? args[2] : lastArg.efData.flags.dae.itemData.data.ability;
const saveDC = getSaveDC(ability, cToken.actor);

async function saveHalfDamage(target, saveType, spellLevel, magicResistance) {
  let diceType = 'd8';
  let diceRolls = '';
  let damageRoll = new Roll(`${spellLevel}${diceType}`).roll();
  for (let diceRoll of damageRoll.terms[0].results) diceRolls += `<li class="roll die ${diceType} min">${diceRoll.result}</li>`;
  let hasAdvantage = (magicResistance) ? ' (Advantage)' : '';
  let saveRoll = (magicResistance) ? (await target.actor.rollAbilitySave(saveType, {fastForward: true, advantage: true, chatMessage: true}))?.total : (await target.actor.rollAbilitySave(saveType, {fastForward: true, chatMessage: true}))?.total;
  if (!saveRoll) return;

  if (saveRoll >= saveDC) {
    let content = `<div class="dnd5e chat-card item-card midi-qol-item-card" data-actor-id="${cToken.actor.id}" data-item-id="${lastArg.efData.flags.dae.itemData._id}" data-spell-level="${spellLevel}">
                      <header class="card-header flexrow">
                        <img src="${lastArg.efData.icon}" title="${lastArg.efData.label}" width="36" height="36" />
                        <h3 class="item-name">${lastArg.efData.label}</h3>
                      </header>

                      <div class="card-content">${lastArg.efData.flags.dae.itemData.data.description.value}</div>

                      <div class="card-buttons">
                        <div>
                          <div class="flexrow 1">
                            <div class="midi-qol-attack-roll">
                              <div class="end-midi-qol-attack-roll"></div>
                            </div>
                          </div>
                          <div class="flexrow 1">
                            <div class="midi-qol-damage-roll" style="text-align:center;text-transform:capitalize;">
                              (${damageType})
                              <div class="dice-roll">
                                <div class="dice-result">
                                  <div class="dice-formula">${damageRoll.formula}</div>
                                  <div class="dice-tooltip">
                                    <div class="dice">
                                      <header class="part-header flexrow">
                                        <span class="part-formula">${spellLevel}${diceType}</span>
                                        <span class="part-total">${spellLevel}</span>
                                      </header>
                                      <ol class="dice-rolls">${diceRolls}</ol>
                                    </div>
                                  </div>
                                  <h4 class="dice-total">${damageRoll.total}</h4>
                                </div>
                              </div>
                              <div class="end-midi-qol-damage-roll"></div>
                            </div>
                          </div>
                          <div class="flexrow 1">
                            <div class="midi-qol-other-roll">
                              <div class="end-midi-qol-other-roll">
                            </div>
                            <div class="midi-qol-hits-display">
                              <div class="end-midi-qol-hits-display"></div>
                            </div>
                          </div>
                        </div>
                        <div class="midi-qol-saves-display">
                          <div data-item-id="${lastArg.efData.flags.dae.itemData._id}">
                            <div class="midi-qol-nobox midi-qol-bigger-text">${lastArg.efData.label} DC ${saveDC} ${CONFIG.DND5E.abilities[saveType]} Saving Throw${hasAdvantage}:</div>
                            <div>
                              <div class="midi-qol-nobox">
                                <div class="midi-qol-bigger-text">
                                  <span>Half damage on save</span>
                                </div>
                                <div class="midi-qol-flex-container">
                                  <div class="midi-qol-target-npc midi-qol-target-name" id="${target.id}">${target.name}</div> <div>saves with ${saveRoll}</div> <div><img src="${target.data.img}" width="30" height="30" style="border:0px" /></div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="end-midi-qol-saves-display"></div>
                        </div>
                      </div>

                    <footer class="card-footer" style="padding: 0px;"></footer>
                  </div>`;
    ChatMessage.create({user: game.user._id, speaker: ChatMessage.getSpeaker({token: target}), content});
    MidiQOL.applyTokenDamage([{damage: Math.floor(damageRoll.total * 0.5), type: damageType}], damageRoll.results, new Set([target]), lastArg.efData.flags.dae.itemData.name, new Set());
  }
  else {
    let content = `<div class="dnd5e chat-card item-card midi-qol-item-card" data-actor-id="${cToken.actor.id}" data-item-id="${lastArg.efData.flags.dae.itemData._id}" data-spell-level="${spellLevel}">
                      <header class="card-header flexrow">
                        <img src="${lastArg.efData.icon}" title="${lastArg.efData.label}" width="36" height="36" />
                        <h3 class="item-name">${lastArg.efData.label}</h3>
                      </header>

                      <div class="card-content">${lastArg.efData.flags.dae.itemData.data.description.value}</div>

                      <div class="card-buttons">
                        <div>
                          <div class="flexrow 1">
                            <div class="midi-qol-attack-roll">
                              <div class="end-midi-qol-attack-roll"></div>
                            </div>
                          </div>
                          <div class="flexrow 1">
                            <div class="midi-qol-damage-roll" style="text-align:center;text-transform:capitalize;">
                              (${damageType})
                              <div class="dice-roll">
                                <div class="dice-result">
                                  <div class="dice-formula">${damageRoll.formula}</div>
                                  <div class="dice-tooltip">
                                    <div class="dice">
                                      <header class="part-header flexrow">
                                        <span class="part-formula">${spellLevel}${diceType}</span>
                                        <span class="part-total">${spellLevel}</span>
                                      </header>
                                      <ol class="dice-rolls">${diceRolls}</ol>
                                    </div>
                                  </div>
                                  <h4 class="dice-total">${damageRoll.total}</h4>
                                </div>
                              </div>
                              <div class="end-midi-qol-damage-roll"></div>
                            </div>
                          </div>
                          <div class="flexrow 1">
                            <div class="midi-qol-other-roll">
                              <div class="end-midi-qol-other-roll">
                            </div>
                            <div class="midi-qol-hits-display">
                              <div class="end-midi-qol-hits-display"></div>
                            </div>
                          </div>
                        </div>
                        <div class="midi-qol-saves-display">
                          <div data-item-id="${lastArg.efData.flags.dae.itemData._id}">
                            <div class="midi-qol-nobox midi-qol-bigger-text">${lastArg.efData.label} DC ${saveDC} ${CONFIG.DND5E.abilities[saveType]} Saving Throw${hasAdvantage}:</div>
                            <div>
                              <div class="midi-qol-nobox">
                                <div class="midi-qol-bigger-text">
                                  <span>Half damage on save</span>
                                </div>
                                <div class="midi-qol-flex-container">
                                  <div class="midi-qol-target-npc midi-qol-target-name" id="${target.id}">${target.name}</div> <div>fails with ${saveRoll}</div> <div><img src="${target.data.img}" width="30" height="30" style="border:0px" /></div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="end-midi-qol-saves-display"></div>
                        </div>
                      </div>

                    <footer class="card-footer" style="padding: 0px;"></footer>
                  </div>`;
    ChatMessage.create({user: game.user._id, speaker: ChatMessage.getSpeaker({token: target}), content});
    MidiQOL.applyTokenDamage([{damage: damageRoll.total, type: damageType}], damageRoll.results, new Set([target]), lastArg.efData.flags.dae.itemData.name, new Set());
  }
}

if (args[0] === "on" && isCaster) {
  let params = [{
    filterType: "field",
    filterId: "spiritGuardians",
    shieldType: 8,
    gridPadding: 5.0,
    color: 0xC0C0C0,
    time: 0,
    blend: 14,
    intensity: 1,
    lightAlpha: 0.1,
    lightSize: 0.1,
    scale: 1,
    radius: 1,
    chromatic: false,
    discardThreshold: 0.57,
    alphaDiscard: true,
    animated:
    {
      time:
      {
        active: true,
        speed: 0.0021,
        animType: "move"
      },
      radius:
      {
        active: true,
        loopDuration: 3000,
        animType: "cosOscillation",
        val1: 0.8,
        val2: 0.8
      }
    }
  }];
  TokenMagic.addFilters(cToken, params);
}

if ((args[0] === "on" || args[0] === "each") && !isCaster) {
  let immuneDI = [damageType].some(di => (target.actor.data.data.traits.di.value || "").includes(di));
  if (target.actor.data.data.attributes.hp.value <= 0 || immuneDI) return;
  else {
    let spellLevel = parseInt(args[3]);
    let magicResistance = (target.actor.data.items?.filter(a => a.type === "feat" && a.name === "Magic Resistance")?.length > 0);
    saveHalfDamage(target, "wis", spellLevel, magicResistance);
  }
}

if (args[0] === "off" && isCaster) {
  if (TokenMagic.hasFilterId(cToken, "spiritGuardians")) await TokenMagic.deleteFilters(cToken, "spiritGuardians");
  if (game.cub.hasCondition("Concentrating", cToken, {warn: false})) await game.cub.removeCondition("Concentrating", cToken, {warn: false});
}
