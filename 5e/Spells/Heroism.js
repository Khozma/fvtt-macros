// Heroism Macro
// Usage: Macro => Execute "Heroism Macro" @token @attributes.spellcasting
// Set macro repeat to start of each turn. Based on a macro by Kandashi from the DAE SRD.
const lastArg = args[args.length-1];
let caster = canvas.tokens.get(args[1]);
let casterActor = caster.actor;
let target = canvas.tokens.get(lastArg.tokenId);
let item = lastArg.efData.flags.dae.itemData;

function findAbilityMod(ability, actor) {
  if (ability === "str") return actor.data.data.abilities.str.mod;
  else if (ability === "dex") return actor.data.data.abilities.dex.mod;
  else if (ability === "con") return actor.data.data.abilities.con.mod;
  else if (ability === "int") return actor.data.data.abilities.int.mod;
  else if (ability === "wis") return actor.data.data.abilities.wis.mod;
  else if (ability === "cha") return actor.data.data.abilities.cha.mod;
  else return 1;
}
let ability = (item.data.ability !== "") ? item.data.ability : args[2];
let abilityMod = findAbilityMod(ability, casterActor);

if (args[0] === "each") {
  let tempHp = (Number(target.actor.data.data.attributes.hp.temp) || 0);
  if (tempHp > abilityMod) return;
  else {
    target.actor.update({"data.attributes.hp.temp": abilityMod});
    let content = `Heroism gives ${target.name} <strong>${abilityMod}</strong> temporary hit points.`;
    ChatMessage.create({user: game.user._id, speaker: ChatMessage.getSpeaker({token: caster}), content});
  }
}

if (args[0] === "off") {
  if (target) {
    let tempHp = (Number(target.actor.data.data.attributes.hp.temp) || 0);
    if (tempHp <= abilityMod) target.actor.update({"data.attributes.hp.temp": null});
  }
}
