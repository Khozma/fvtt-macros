// Produce Flame OnUse ItemMacro
// See other Produce Flame macro for instructions. This self-deletes the item and ends the spell's effect on use.
let cToken = canvas.tokens.get(args[0].tokenId);
let cActor = (cToken) ? cToken.actor : game.actors.get(args[0].actor._id);
let efId = cActor.effects?.entries.find(ef => ef.data.label === "Produce Flame")?.id;
if (efId) await cActor.deleteEmbeddedEntity("ActiveEffect", efId);
