// DAE macro. Thanks to tposney for this one and helping out by simplifying an older macro of mine.
// Run on macro.execute and set macro repeat to start of each turn.
if (args[0] ==="each") {
  const lastArg = args[args.length-1];
  const ttoken = canvas.tokens.get(lastArg.tokenId);
  const tactor = ttoken.actor;
  let content = `Due to Tasha's Mind Whip, ${tactor.name} must choose a move, an action, or a bonus action; only getting one of the three.`;
  ChatMessage.create({user: game.user._id, speaker: ChatMessage.getSpeaker({token: ttoken}), content});
  await tactor.deleteEmbeddedEntity("ActiveEffect", lastArg.effectId);
}
