// Blinding Smite Macro
// Usage: Macro => Execute "Blinding Smite Macro" @target @actor @item @attributes.spelldc
// Put it on a spell with target 1 creature, action type other, damage, and a constitution save. Set macro repeat to end of each turn.
const lastArg = args[args.length-1];
let target = canvas.tokens.get(args[1]);
if (lastArg.tokenId) target = canvas.tokens.get(lastArg.tokenId);
const item = args[3];
const spellDC = (Number(item.data.save.dc) || parseInt(args[4]));
const saveType = "con";
const saveTypeS = "Constitution";
let immuneCI;
if (target) immuneCI = ["blinded"].some(ci => (target.actor.data.data.traits.ci.value || "").includes(ci));

async function conSave(target) {
  let saveRoll = await target.actor.rollAbilitySave(saveType, {fastForward: true, chatMessage: true});
  if (saveRoll === null) return;

  if (saveRoll.total < spellDC) {
    let results_html = `<div class="dnd5e chat-card item-card">
                          <header class="card-header flexrow">
                              <img src="${item.img}" title="${item.name}" width="36" height="36" />
                              <h3 class="item-name">${item.name}</h3>
                          </header>

                          <div class="card-content">${item.data.description.value}</div>

                          <div class="card-buttons">
                              <div class="midi-qol-saves-display">
                                <div class="midi-qol-nobox midi-qol-bigger-text">${item.name} DC ${spellDC} ${saveTypeS} Saving Throw:</div>
                                <div>
                                    <div class="midi-qol-nobox">
                                        <div class="midi-qol-flex-container">
                                            <div class="midi-qol-target-npc midi-qol-target-name" id="${target.id}">${target.name}</div> <div>fails with ${saveRoll.total}</div> <div><img src="${target.data.img}" width="30" height="30" style="border:0px" /></div>
                                        </div>
                                    </div>
                                </div>
                              </div> 
                          </div>

                          <footer class="card-footer" style="padding: 0px;"></footer>
                        </div>`;
    ChatMessage.create({user: game.user._id, speaker: ChatMessage.getSpeaker({token: target}), content: results_html});
  }
  else if (saveRoll.total >= spellDC) {
    let results_html = `<div class="dnd5e chat-card item-card">
                          <header class="card-header flexrow">
                              <img src="${item.img}" title="${item.name}" width="36" height="36" />
                              <h3 class="item-name">${item.name}</h3>
                          </header>

                          <div class="card-content">${item.data.description.value}</div>

                          <div class="card-buttons">
                              <div class="midi-qol-saves-display">
                                <div class="midi-qol-nobox midi-qol-bigger-text">${item.name} DC ${spellDC} ${saveTypeS} Saving Throw:</div>
                                <div>
                                    <div class="midi-qol-nobox">
                                        <div class="midi-qol-flex-container">
                                            <div class="midi-qol-target-npc midi-qol-target-name" id="${target.id}">${target.name}</div> <div>saves with ${saveRoll.total}</div> <div><img src="${target.data.img}" width="30" height="30" style="border:0px" /></div>
                                        </div>
                                    </div>
                                </div>
                              </div> 
                          </div>

                          <footer class="card-footer" style="padding: 0px;"></footer>
                        </div>`;
    ChatMessage.create({user: game.user._id, speaker: ChatMessage.getSpeaker({token: target}), content: results_html});
    removeEffect(target);
  }
}

async function removeEffect(target) {
  await target.actor.deleteEmbeddedEntity("ActiveEffect", lastArg.effectId);
}

if (args[0] === "on") {
  if (!immuneCI) await game.cub.addCondition("Blinded", target);
  await game.macros.getName("Concentrating").execute("on", args[2], args[1], args[3]);
}

if (args[0] === "each") {
  if (target.actor.data.data.attributes.hp.value <= 0 || immuneCI) {
    if (game.cub.hasCondition("Blinded", target)) await game.cub.removeCondition("Blinded", target);
    removeEffect(target);
  }
  else conSave(target);
}

if (args[0] === "off") {
	await game.macros.getName("Concentrating").execute("off", args[2]);
}
