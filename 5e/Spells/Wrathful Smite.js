// Wrathful Smite Macro
// Usage: Macro => Execute "Wrathful Smite Macro" @token @attributes.spelldc
// Put it on a spell with target 1 creature, action type other, damage, and a wisdom save. Set macro repeat to start of each turn.
const lastArg = args[args.length-1];
let target = canvas.tokens.get(lastArg.tokenId);
let cToken = canvas.tokens.get(args[1]);
let item = lastArg.efData.flags.dae.itemData;
const spellDC = (Number(item.data.save.dc) || parseInt(args[2]));
let immuneCI;
if (target) immuneCI = ["frightened"].some(ci => (target.actor.data.data.traits.ci.value || "").includes(ci));

async function abilityCheck(target, checkType) {
  let abilityRoll = (await target.actor.rollAbilityTest(checkType, {fastForward: true, chatMessage: true}));
  if (abilityRoll === null) return;

  if (abilityRoll.total >= spellDC) {
    let content = `<div class="dnd5e chat-card item-card midi-qol-item-card" data-actor-id="${cToken.actor.id}" data-item-id="${item._id}" data-spell-level="">
                    <header class="card-header flexrow">
                      <img src="${item.img}" title="${item.name}" width="36" height="36" />
                      <h3 class="item-name">${item.name}</h3>
                    </header>

                    <div class="card-content">${item.data.description.value}</div>

                    <div class="card-buttons">
                      <div>
                        <div class="flexrow 1">
                          <div class="midi-qol-attack-roll">
                            <div class="end-midi-qol-attack-roll"></div>
                          </div>
                        </div>
                        <div class="flexrow 1">
                          <div class="midi-qol-damage-roll" style="text-align:center;text-transform:capitalize;">
                            <div class="end-midi-qol-damage-roll"></div>
                          </div>
                        </div>
                        <div class="flexrow 1">
                          <div class="midi-qol-other-roll">
                            <div class="end-midi-qol-other-roll">
                          </div>
                          <div class="midi-qol-hits-display">
                            <div class="end-midi-qol-hits-display"></div>
                          </div>
                        </div>
                      </div>
                      <div class="midi-qol-saves-display">
                        <div data-item-id="${item._id}">
                          <div class="midi-qol-nobox midi-qol-bigger-text">${item.name} DC ${spellDC} ${CONFIG.DND5E.abilities[checkType]} Ability Check:</div>
                          <div>
                            <div class="midi-qol-nobox">
                              <div class="midi-qol-flex-container">
                                  <div class="midi-qol-target-npc midi-qol-target-name" id="${target.id}">${target.name}</div> <div>succeeds with ${abilityRoll.total}</div> <div><img src="${target.data.img}" width="30" height="30" style="border:0px" /></div>
                                </div>
                            </div>
                          </div>
                        </div>
                        <div class="end-midi-qol-saves-display"></div>
                      </div>
                    </div>

                    <footer class="card-footer" style="padding: 0px;"></footer>
                  </div>`;
    ChatMessage.create({user: game.user._id, speaker: ChatMessage.getSpeaker({token: target}), content});
    removeEffect(target);
  }
  else {
    let content = `<div class="dnd5e chat-card item-card midi-qol-item-card" data-actor-id="${cToken.actor.id}" data-item-id="${item._id}" data-spell-level="">
                    <header class="card-header flexrow">
                      <img src="${item.img}" title="${item.name}" width="36" height="36" />
                      <h3 class="item-name">${item.name}</h3>
                    </header>

                    <div class="card-content">${item.data.description.value}</div>

                    <div class="card-buttons">
                      <div>
                        <div class="flexrow 1">
                          <div class="midi-qol-attack-roll">
                            <div class="end-midi-qol-attack-roll"></div>
                          </div>
                        </div>
                        <div class="flexrow 1">
                          <div class="midi-qol-damage-roll" style="text-align:center;text-transform:capitalize;">
                            <div class="end-midi-qol-damage-roll"></div>
                          </div>
                        </div>
                        <div class="flexrow 1">
                          <div class="midi-qol-other-roll">
                            <div class="end-midi-qol-other-roll">
                          </div>
                          <div class="midi-qol-hits-display">
                            <div class="end-midi-qol-hits-display"></div>
                          </div>
                        </div>
                      </div>
                      <div class="midi-qol-saves-display">
                        <div data-item-id="${item._id}">
                          <div class="midi-qol-nobox midi-qol-bigger-text">${item.name} DC ${spellDC} ${CONFIG.DND5E.abilities[checkType]} Ability Check:</div>
                          <div>
                            <div class="midi-qol-nobox">
                              <div class="midi-qol-flex-container">
                                  <div class="midi-qol-target-npc midi-qol-target-name" id="${target.id}">${target.name}</div> <div>fails with ${abilityRoll.total}</div> <div><img src="${target.data.img}" width="30" height="30" style="border:0px" /></div>
                                </div>
                            </div>
                          </div>
                        </div>
                        <div class="end-midi-qol-saves-display"></div>
                      </div>
                    </div>

                    <footer class="card-footer" style="padding: 0px;"></footer>
                  </div>`;
    ChatMessage.create({user: game.user._id, speaker: ChatMessage.getSpeaker({token: target}), content});
  }
}

async function removeEffect(target) {
  await target.actor.deleteEmbeddedEntity("ActiveEffect", lastArg.effectId);
}

if (args[0] === "each") {
  if (target.actor.data.data.attributes.hp.value <= 0 || immuneCI) removeEffect(target);
  else abilityCheck(target, "wis");
}

if (args[0] === "off") {
  if (game.cub.hasCondition("Concentrating", cToken, {warn: false})) await game.cub.removeCondition("Concentrating", cToken, {warn: false});
}
