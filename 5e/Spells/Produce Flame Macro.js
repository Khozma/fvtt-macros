// Produce Flame Macro | Requires DAE/MidiQOL
// Usage: Macro => Execute "Produce Flame Macro"
// Set the Produce Flame cantrip to target Self and use this DAE macro with it.
// Configure a world item of type Weapon with name "Produce Flame", and set its OnUse macro to the other Produce Flame OnUse ItemMacro.
const lastArg = args[args.length-1];
let name = "Produce Flame";
let type = "weapon";
const createItem = {name, type};
const item = lastArg.efData.flags.dae.itemData;
let cToken = canvas.tokens.get(lastArg.tokenId);
let cActor = (cToken) ? cToken.actor : game.actors.get(lastArg.actorId);

if (args[0] === "on") {
	const filterItem = game.items.filter(i => i.type === createItem.type && i.name === createItem.name);
	if (filterItem?.length > 0) {
		const copyItem = duplicate(filterItem[0]);
		let ability = (item.data.ability !== "") ? item.data.ability : cActor.data.data.attributes.spellcasting;
		let level = (cActor.data.type === "character") ? cActor.data.data.details.level : cActor.data.data.details.spellLevel;
		let numDice = 1 + Math.floor((level + 1) / 6);
		let damageFormula = `${numDice}d8 + @mod`;
		copyItem.name = item.name;
		copyItem.data.description.value = item.data.description.value;
		copyItem.data.ability = ability;
		copyItem.data.damage.parts[0][0] = damageFormula;
		copyItem.data.equipped = true;
		copyItem.data.proficient = true;
		cActor.createOwnedItem(copyItem);
	}
}

if (args[0] === "off") {
	if (cActor.items?.filter(i => i.type === createItem.type && i.name === item.name)?.length > 0) await cActor.deleteOwnedItem(cActor.items.filter(i => i.type === createItem.type && i.name === item.name).map(i => i._id));
}
