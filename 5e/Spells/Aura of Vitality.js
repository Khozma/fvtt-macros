// Aura of Vitality Macro
// Usage: Macro => Execute "Aura of Vitality Macro" @token
// Create a world item with type Feature that heals 2d6 HP as a bonus action. This macro will add/remove it from the caster.
const lastArg = args[args.length-1];
const itemName = "Aura of Vitality"; // The name of the item created/deleted by the macro
let cToken = canvas.tokens.get(args[1]);
let cActor = (cToken) ? cToken.actor : game.actors.get(lastArg.actorId);

if (args[0] === "on") {
	const filterItem = game.items.filter(i => i.type === "feat" && i.name === itemName);
	if (filterItem?.length > 0) {
		let copyItem = duplicate(filterItem[0]);
		cActor.createOwnedItem(copyItem);
	}
}

if (args[0] === "off") {
	if (cActor.items?.filter(i => i.type === "feat" && i.name === itemName)?.length > 0) await cActor.deleteOwnedItem(cActor.items.filter(i => i.type === "feat" && i.name === itemName).map(i => i._id));
}
