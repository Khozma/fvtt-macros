// Searing Smite Macro
// Usage: Macro => Execute "Searing Smite Macro" @token @attributes.spelldc
// Put it on a spell with target 1 creature, action type other, fire damage, and a constitution save. Set macro repeat to start of each turn.
const lastArg = args[args.length-1];
let target = canvas.tokens.get(lastArg.tokenId);
let cToken = canvas.tokens.get(args[1]);
let item = lastArg.efData.flags.dae.itemData;
const spellDC = (Number(item.data.save.dc) || parseInt(args[2]));
const damageType = item.data.damage.parts[0][1];
let immuneDI;
if (target) immuneDI = [damageType].some(di => (target.actor.data.data.traits.di.value || "").includes(di));

async function TMfxFire(target, state) {
  if (state === "on") {
    let params = [{
      filterType: "ripples",
      filterId: "searingSmite",
      color: 0xCC9000,
      time: 0,
      alphaDiscard: false,
      animated:
      {
        time: 
        { 
          active: true, 
          speed: 0.0009, 
          animType: "move" 
        }
      }
    }];
    TokenMagic.addUpdateFilters(target, params);
  }
  if (state === "off" && TokenMagic.hasFilterId(target, "searingSmite")) {
    await TokenMagic.deleteFilters(target, "searingSmite");
  }
}

async function save(target, saveType) {
  let numDice = 1;
  let diceType = 'd6';
  let diceRolls = '';
  let damageRoll = new Roll(`${numDice}${diceType}`).roll();
  for (let diceRoll of damageRoll.terms[0].results) diceRolls += `<li class="roll die ${diceType} min">${diceRoll.result}</li>`;
  let saveRoll = (await target.actor.rollAbilitySave(saveType, {fastForward: true, chatMessage: true}));
  if (saveRoll === null) return;

  if (saveRoll.total >= spellDC) {
    let results_html = `<div class="dnd5e chat-card item-card midi-qol-item-card" data-actor-id="${cToken.actor.id}" data-item-id="${item._id}" data-spell-level="">
                          <header class="card-header flexrow">
                            <img src="${item.img}" title="${item.name}" width="36" height="36" />
                            <h3 class="item-name">${item.name}</h3>
                          </header>

                          <div class="card-content">${item.data.description.value}</div>

                          <div class="card-buttons">
                            <div>
                              <div class="flexrow 1">
                                <div class="midi-qol-attack-roll">
                                  <div class="end-midi-qol-attack-roll"></div>
                                </div>
                              </div>
                              <div class="flexrow 1">
                                <div class="midi-qol-damage-roll" style="text-align:center;text-transform:capitalize;">
                                  (${damageType})
                                  <div class="dice-roll">
                                    <div class="dice-result">
                                      <div class="dice-formula">${damageRoll.formula}</div>
                                      <div class="dice-tooltip">
                                        <div class="dice">
                                          <header class="part-header flexrow">
                                            <span class="part-formula">${numDice}${diceType}</span>
                                            <span class="part-total">${numDice}</span>
                                          </header>
                                          <ol class="dice-rolls">${diceRolls}</ol>
                                        </div>
                                      </div>
                                      <h4 class="dice-total">${damageRoll.total}</h4>
                                    </div>
                                  </div>
                                  <div class="end-midi-qol-damage-roll"></div>
                                </div>
                              </div>
                              <div class="flexrow 1">
                                <div class="midi-qol-other-roll">
                                  <div class="end-midi-qol-other-roll">
                                </div>
                                <div class="midi-qol-hits-display">
                                  <div class="end-midi-qol-hits-display"></div>
                                </div>
                              </div>
                            </div>
                            <div class="midi-qol-saves-display">
                              <div data-item-id="${item._id}">
                                <div class="midi-qol-nobox midi-qol-bigger-text">${item.name} DC ${spellDC} ${CONFIG.DND5E.abilities[saveType]} Saving Throw:</div>
                                <div>
                                  <div class="midi-qol-nobox">
                                    <div class="midi-qol-bigger-text">
                                      <span>No damage on save</span>
                                    </div>
                                    <div class="midi-qol-flex-container">
                                      <div class="midi-qol-target-npc midi-qol-target-name" id="${target.id}">${target.name}</div> <div>saves with ${saveRoll.total}</div> <div><img src="${target.data.img}" width="30" height="30" style="border:0px" /></div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="end-midi-qol-saves-display"></div>
                            </div>
                          </div>

                          <footer class="card-footer" style="padding: 0px;"></footer>
                        </div>`;
    ChatMessage.create({user: game.user._id, speaker: ChatMessage.getSpeaker({token: target}), content: results_html});
    removeEffect(target);
  }
  else {
    let results_html = `<div class="dnd5e chat-card item-card midi-qol-item-card" data-actor-id="${cToken.actor.id}" data-item-id="${item._id}" data-spell-level="">
                          <header class="card-header flexrow">
                            <img src="${item.img}" title="${item.name}" width="36" height="36" />
                            <h3 class="item-name">${item.name}</h3>
                          </header>

                          <div class="card-content">${item.data.description.value}</div>

                          <div class="card-buttons">
                            <div>
                              <div class="flexrow 1">
                                <div class="midi-qol-attack-roll">
                                  <div class="end-midi-qol-attack-roll"></div>
                                </div>
                              </div>
                              <div class="flexrow 1">
                                <div class="midi-qol-damage-roll" style="text-align:center;text-transform:capitalize;">
                                  (${damageType})
                                  <div class="dice-roll">
                                    <div class="dice-result">
                                      <div class="dice-formula">${damageRoll.formula}</div>
                                      <div class="dice-tooltip">
                                        <div class="dice">
                                          <header class="part-header flexrow">
                                            <span class="part-formula">${numDice}${diceType}</span>
                                            <span class="part-total">${numDice}</span>
                                          </header>
                                          <ol class="dice-rolls">${diceRolls}</ol>
                                        </div>
                                      </div>
                                      <h4 class="dice-total">${damageRoll.total}</h4>
                                    </div>
                                  </div>
                                  <div class="end-midi-qol-damage-roll"></div>
                                </div>
                              </div>
                              <div class="flexrow 1">
                                <div class="midi-qol-other-roll">
                                  <div class="end-midi-qol-other-roll">
                                </div>
                                <div class="midi-qol-hits-display">
                                  <div class="end-midi-qol-hits-display"></div>
                                </div>
                              </div>
                            </div>
                            <div class="midi-qol-saves-display">
                              <div data-item-id="${item._id}">
                                <div class="midi-qol-nobox midi-qol-bigger-text">${item.name} DC ${spellDC} ${CONFIG.DND5E.abilities[saveType]} Saving Throw:</div>
                                <div>
                                  <div class="midi-qol-nobox">
                                    <div class="midi-qol-bigger-text">
                                      <span>No damage on save</span>
                                    </div>
                                    <div class="midi-qol-flex-container">
                                      <div class="midi-qol-target-npc midi-qol-target-name" id="${target.id}">${target.name}</div> <div>fails with ${saveRoll.total}</div> <div><img src="${target.data.img}" width="30" height="30" style="border:0px" /></div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="end-midi-qol-saves-display"></div>
                            </div>
                          </div>

                          <footer class="card-footer" style="padding: 0px;"></footer>
                        </div>`;
    ChatMessage.create({user: game.user._id, speaker: ChatMessage.getSpeaker({token: target}), content: results_html});
    MidiQOL.applyTokenDamage([{damage: damageRoll.total, type: damageType}], damageRoll.results, new Set([target]), item.name, new Set());
  }
}

async function removeEffect(target) {
  await target.actor.deleteEmbeddedEntity("ActiveEffect", lastArg.effectId);
}

if (args[0] === "on" && !immuneDI) {
  if (damageType === "fire") TMfxFire(target, "on");
}

if (args[0] === "each") {
  if (target.actor.data.data.attributes.hp.value <= 0 || immuneDI) removeEffect(target);
  else save(target, "con");
}

if (args[0] === "off") {
  if (damageType === "fire") TMfxFire(target, "off");
  let caster = canvas.tokens.get(args[1]);
  if (game.cub.hasCondition("Concentrating", caster, {warn: false})) await game.cub.removeCondition("Concentrating", caster, {warn: false});
}
