// Banish MidiQOL onUse/ItemMacro
if (args[0].targets.length === 0) return ui.notifications.warn(`No target was selected.`);
//console.log(args[0]);
if (args[0].failedSaves.length === 0) return;
let failedSaves = args[0].failedSaves;
for (let target of failedSaves) {
	if (target._id !== args[0].tokenId) {
		let targetToken = canvas.tokens.get(target._id);
		targetToken.update({"hidden": true});
	}
}
