// Aura of Life Macro
// Usage: Macro => Execute "Aura of Life Macro"
// Set macro repeat to start of each turn.
const lastArg = args[args.length-1];
let target = canvas.tokens.get(lastArg.tokenId);

if (args[0] === "each" && (target.actor.data.data.attributes.hp.value <= 0)) {
  let undead = (target.actor.data.data.details.type || "").toLowerCase().includes(["undead"]);
  if (!undead) {
    target.actor.update({"data.attributes.hp.value": 1});
    let content = `${target.name} regains <strong>1</strong> hit point from ${lastArg.efData.flags.dae.itemData.name}.`;
    ChatMessage.create({user: game.user._id, speaker: ChatMessage.getSpeaker({token: target}), content});
  }
}
