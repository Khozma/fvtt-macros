// Life Drain | MidiQOL onUse Macro
// A (mostly) one-size-fits-all world macro for monsters that have HP maximum reduction effects.
// Creates/updates an effect reducing the target's HP maximum by the necrotic or psychic damage dealt.
// Effect on undead (wights, wraiths, vampires), demons (chasmes, succubi), yugoloths (yagnoloths), etc.
(async ()=>{
	if (args[0].targets.length === 0 || args[0].hitTargets.length === 0) return;
	let attacker = canvas.tokens.get(args[0].tokenId);
	let target = canvas.tokens.get(args[0].hitTargets[0]._id);
	let item = args[0].item;
	const useEffect = false; // Always use active effect if true, or use temporary max HP except on NPCs if false

	// Find the damage, attacker's creature type, and target's immunities
	function findLifeDrain() {
		if (args[0].damageDetail[0].type === "necrotic" || args[0].damageDetail[0].type === "psychic") {return args[0].damageDetail[0];}
		else if (args[0].damageDetail[1].type === "necrotic" || args[0].damageDetail[1].type === "psychic") {return args[0].damageDetail[1];}
		else {return args[0].damageDetail[0];}
	}
	const lifeDrain = findLifeDrain();
	let undead = (attacker.actor.data.data.details.type || "").toLowerCase().includes(["undead"]);
	let yugoloth = (attacker.actor.data.data.details.type || "").toLowerCase().includes(["yugoloth"]);
	let immuneCI = ["life drain"].some(ci => (target.actor.data.data.traits.ci.custom || "").toLowerCase().includes(ci)) || ["exhaustion"].some(ci => (target.actor.data.data.traits.ci.value || "").includes(ci));
	let immuneDI = [lifeDrain.type].some(di => (target.actor.data.data.traits.di.value || "").includes(di));
	let object = (target.actor.data.data.details.type || "").toLowerCase().includes(["object"]) || (target.actor.data.type !== "character" && target.actor.data.type !== "npc"); // not a great way to check; Foundry has no proper support for objects in 5e
	//console.log(`Life Drain | ${args[0].actor.name} ${item.name} hit ${target.actor.data.name} for ${lifeDrain.damage} ${lifeDrain.type} damage | ${target.actor.data.name} {CI: ${immuneCI}, ${lifeDrain.type} DI: ${immuneDI}} | ${args[0].actor.name} type: ${attacker.actor.data.data.details.type} {undead: ${undead}, yugoloth: ${yugoloth}} | object: ${object}`);

	if (!immuneCI && !immuneDI && !object) {
		// Self-heal when the damage is secondary and necrotic (e.g., as it is on vampires)
		if (undead && (args[0].damageDetail[0].type !== lifeDrain.type) && (args[0].damageDetail[1].type === "necrotic")) {
			MidiQOL.applyTokenDamage([{damage: lifeDrain.damage, type: "healing"}], lifeDrain.damage, new Set([attacker]), item.name, new Set());
		}

		// For life leech of yugoloths (e.g., as on yagnoloths)
		if (yugoloth && (args[0].damageDetail[0].type === lifeDrain.type)) {
			let tempHP = Math.floor(lifeDrain.damage * 0.5);
			await attacker.actor.update({"data.attributes.hp.temp": tempHP})
		}

		// Only apply the effect if the target has either failed the save or the damage is secondary
		if (args[0].damageDetail[0].type === lifeDrain.type && args[0].failedSaves.length === 0) return;
		else if (useEffect == true || target.actor.data.type === "npc") {
			// Effect label and icon
			let label = "Life Drain";
			let icon = "systems/dnd5e/icons/skills/shadow_18.jpg";

			// Update the effect on the target if already present
			let effect = target.actor.effects?.find(i => i?.data.label === label);
			if (effect !== null) {
				let changes = effect.data.changes;
				let effectHpMax = (Number(changes[0].value)) - lifeDrain.damage;
			  changes[0].value = effectHpMax;
			  //console.log(`Life Drain | Updating ActiveEffect on ${target.name}:`, effect, changes);
			  await effect.update({changes});
			}
			else { // Otherwise, create the effect
				const effectData = {
			    label,
			    icon,
			    origin: `Actor.${args[0].actor._id}.OwnedItem.${args[0].item._id}`,
			    changes: [{
			      key: "data.attributes.hp.max",
			      mode: 2,
			      value: -lifeDrain.damage,
			      priority: 20
			    }]
				}
				//console.log(`Life Drain | Creating ActiveEffect on ${target.name}:`, effectData);
				await target.actor.createEmbeddedEntity("ActiveEffect", effectData);
			}
		}
		else if (useEffect == false && target.actor.data.type === "character") {
			let hpTempMax = (Number(target.actor.data.data.attributes.hp.tempmax) || 0) - lifeDrain.damage;
			await target.actor.update({"data.attributes.hp.tempmax": hpTempMax})
		}
	}
})();
