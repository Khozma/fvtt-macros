// This is 2 separate simple item macros for the assassin vine's constrict and entangling vines attacks.
// You can also easily change these for a lot of similar monster abilities.

// 1. Constrict:
(async()=>{
	if (args[0].targets.length === 0) return ui.notifications.warn(`No target was selected.`);
	if (args[0].hitTargets.length === 0) return;
	let attacker = canvas.tokens.get(args[0].tokenId);
	let target = canvas.tokens.get(args[0].hitTargets[0]._id);
  if (!game.cub.hasCondition(["Grappled", "Restrained"], target)) {
    await game.cub.addCondition(["Grappled", "Restrained"], target)
    let result = `${target.name} is grappled and restrained by the vine!`;
	  ChatMessage.create({user: game.user._id, speaker: ChatMessage.getSpeaker({token: attacker}), content: result});
  }
})();

// 2. Entangling Vines:
(async()=>{
	if (args[0].targets.length === 0) return ui.notifications.warn(`No target was selected.`);
	if (args[0].failedSaves.length === 0) return;
	let failedSaves = args[0].failedSaves;
	for (let hitTarget of failedSaves) {
		let target = canvas.tokens.get(hitTarget._id);
		if (!game.cub.hasCondition("Restrained", target)) await game.cub.addCondition("Restrained", target)
	}
})();
