// Dretch Fetid Cloud | Requires DAE/CUB/TokenMagic/Active Auras
// Triggers a save on creatures' turns, applying the Poisoned condition on a failed save until the start of that creatures' next turn.
// Put on a feature with target Self, action type Other. Set effect to 60 seconds, macro repeat to start of each turn, aura effect to 15 ft radius.
// Usage: Macro => Execute "Fetid Cloud Macro" @token @abilities.con.dc "poisoned" "con"
const lastArg = args[args.length-1];
let target = canvas.tokens.get(lastArg.tokenId);
const cTokenId = args[1];
let cToken = canvas.tokens.get(cTokenId);
let isSelf = (target.id === cTokenId);
const saveDC = parseInt(args[2]);
const conditionLC = args[3].toLowerCase();
let condition = conditionLC.charAt(0).toUpperCase() + conditionLC.slice(1);
let immuneCI;
if (target) immuneCI = [conditionLC].some(ci => (target.actor.data.data.traits.ci.value || "").includes(ci));

async function savingThrow(target, saveType, condition) {
  let itemNameLC = lastArg.efData.flags.dae.itemData.name.toLowerCase();
  let saveRoll = (await target.actor.rollAbilitySave(saveType, {fastForward: true, chatMessage: true}));
  if (saveRoll === null) return;

  if (saveRoll.total >= saveDC) {
    let content = `<div class="dnd5e chat-card item-card midi-qol-item-card" data-actor-id="${cToken.actor.id}" data-item-id="${lastArg.efData.flags.dae.itemData._id}" data-spell-level="">
                    <header class="card-header flexrow">
                      <img src="${lastArg.efData.icon}" title="${lastArg.efData.label}" width="36" height="36" />
                      <h3 class="item-name">${lastArg.efData.label}</h3>
                    </header>

                    <div class="card-content" style="display: block;">
                      <div class="rd__b  rd__b--2"><p>Any creature that starts its turn in the ${itemNameLC}'s area must succeed on a DC <span class="rd__dc">11</span> Constitution saving throw or be poisoned until the start of its next turn. While poisoned in this way, the target can take either an action or a bonus action on its turn, not both, and can't take reactions.</p></div>
                    </div>

                    <div class="card-buttons">
                      <div>
                        <div class="flexrow 1">
                          <div class="midi-qol-attack-roll">
                            <div class="end-midi-qol-attack-roll"></div>
                          </div>
                        </div>
                        <div class="flexrow 1">
                          <div class="midi-qol-damage-roll" style="text-align:center;text-transform:capitalize;">
                            <div class="end-midi-qol-damage-roll"></div>
                          </div>
                        </div>
                        <div class="flexrow 1">
                          <div class="midi-qol-other-roll">
                            <div class="end-midi-qol-other-roll">
                          </div>
                          <div class="midi-qol-hits-display">
                            <div class="end-midi-qol-hits-display"></div>
                          </div>
                        </div>
                      </div>
                      <div class="midi-qol-saves-display">
                        <div data-item-id="${lastArg.efData.flags.dae.itemData._id}">
                          <div class="midi-qol-nobox midi-qol-bigger-text">${lastArg.efData.label} DC ${saveDC} ${CONFIG.DND5E.abilities[saveType]} Saving Throw:</div>
                          <div>
                            <div class="midi-qol-nobox">
                              <div class="midi-qol-flex-container">
                                <div class="midi-qol-target-npc midi-qol-target-name" id="${target.id}">${target.name}</div> <div>saves with ${saveRoll.total}</div> <div><img src="${target.data.img}" width="30" height="30" style="border:0px" /></div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="end-midi-qol-saves-display"></div>
                      </div>
                    </div>

                    <footer class="card-footer" style="padding: 0px;"></footer>
                  </div>`;
    ChatMessage.create({user: game.user._id, speaker: ChatMessage.getSpeaker({token: target}), content});
  }
  else {
    let content = `<div class="dnd5e chat-card item-card midi-qol-item-card" data-actor-id="${cToken.actor.id}" data-item-id="${lastArg.efData.flags.dae.itemData._id}" data-spell-level="">
                    <header class="card-header flexrow">
                      <img src="${lastArg.efData.icon}" title="${lastArg.efData.label}" width="36" height="36" />
                      <h3 class="item-name">${lastArg.efData.label}</h3>
                    </header>

                    <div class="card-content" style="display: block;">
                      <div class="rd__b  rd__b--2"><p>Any creature that starts its turn in the ${itemNameLC}'s area must succeed on a DC <span class="rd__dc">11</span> Constitution saving throw or be poisoned until the start of its next turn. While poisoned in this way, the target can take either an action or a bonus action on its turn, not both, and can't take reactions.</p></div>
                    </div>

                    <div class="card-buttons">
                      <div>
                        <div class="flexrow 1">
                          <div class="midi-qol-attack-roll">
                            <div class="end-midi-qol-attack-roll"></div>
                          </div>
                        </div>
                        <div class="flexrow 1">
                          <div class="midi-qol-damage-roll" style="text-align:center;text-transform:capitalize;">
                            <div class="end-midi-qol-damage-roll"></div>
                          </div>
                        </div>
                        <div class="flexrow 1">
                          <div class="midi-qol-other-roll">
                            <div class="end-midi-qol-other-roll">
                          </div>
                          <div class="midi-qol-hits-display">
                            <div class="end-midi-qol-hits-display"></div>
                          </div>
                        </div>
                      </div>
                      <div class="midi-qol-saves-display">
                        <div data-item-id="${lastArg.efData.flags.dae.itemData._id}">
                          <div class="midi-qol-nobox midi-qol-bigger-text">${lastArg.efData.label} DC ${saveDC} ${CONFIG.DND5E.abilities[saveType]} Saving Throw:</div>
                          <div>
                            <div class="midi-qol-nobox">
                              <div class="midi-qol-flex-container">
                                <div class="midi-qol-target-npc midi-qol-target-name" id="${target.id}">${target.name}</div> <div>fails with ${saveRoll.total}</div> <div><img src="${target.data.img}" width="30" height="30" style="border:0px" /></div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="end-midi-qol-saves-display"></div>
                      </div>
                    </div>

                    <footer class="card-footer" style="padding: 0px;"></footer>
                  </div>`;
    ChatMessage.create({user: game.user._id, speaker: ChatMessage.getSpeaker({token: target}), content});
    if (!game.cub.hasCondition(condition, target, {warn: false})) game.cub.addCondition(condition, target, {warn: false});
  }
}

if (args[0] === "on" && isSelf) {
  let params = [{
    filterType: "field",
    filterId: "fetidCloud",
    shieldType: 8,
    gridPadding: 3.7,
    color: 0xACC300,
    time: 0,
    blend: 14,
    intensity: 0.8,
    lightAlpha: 0.1,
    lightSize: 0.1,
    scale: 1,
    radius: 1,
    chromatic: false,
    discardThreshold: 0.06,
    alphaDiscard: true,
    animated:
    {
      time:
      {
        active: true,
        speed: 0.0004,
        animType: "move"
      },
      radius: 
      {
        active: true, 
        loopDuration: 3000, 
        animType: "cosOscillation", 
        val1: 0.8, 
        val2: 0.8
      }
    }
  }];
  TokenMagic.addFilters(cToken, params);
}

if (args[0] === "each" && !isSelf) {
  if (target.actor.data.data.attributes.hp.value <= 0 || immuneCI) return;
  else {
    if (game.cub.hasCondition(condition, target, {warn: false})) await game.cub.removeCondition(condition, target, {warn: false});
    savingThrow(target, args[4], condition);
  }
}

if (args[0] === "off") {
  if (isSelf) {
    if (TokenMagic.hasFilterId(cToken, "fetidCloud")) await TokenMagic.deleteFilters(cToken, "fetidCloud");
  }
  else if (target) {
    if (game.cub.hasCondition(condition, target, {warn: false})) await game.cub.removeCondition(condition, target, {warn: false});
  }
}
