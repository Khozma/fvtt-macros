// Shadow Dancer Spiked Chain | MidiQOL onUse/ItemMacro
(async()=>{
	if (args[0].targets.length === 0) return ui.notifications.warn(`No target was selected.`);
	if (!args[0].hitTargets[0] || !args[0].failedSaves[0]) return;
	if (args[0].failedSaves[0].name === args[0].targets[0].name) {
		let dialogContent = `<div style="text-align:center;"><p>Choose an additional effect the target suffers:</p></div>`;
		new Dialog({
			title: "Spiked Chain",
			content: dialogContent,
			buttons: {
				grapple: {
					label: "Grappled", callback: async (html) => {
						let target = canvas.tokens.get(args[0].targets[0]._id);
					  if (!game.cub.hasCondition(["Grappled", "Restrained"], target)) {
					    await game.cub.addCondition(["Grappled", "Restrained"], target)
					    let result = `${target.name} is grappled and restrained!`;
						  ChatMessage.create({user: game.user._id, speaker: ChatMessage.getSpeaker({token: actor}), content: result});
					  }
					}
			  },
				prone: {
					label: "Prone", callback: async (html) => {
						let target = canvas.tokens.get(args[0].targets[0]._id);
					  if (!game.cub.hasCondition("Prone", target)) {
					    await game.cub.addCondition("Prone", target)
					    let result = `${target.name} is knocked prone!`;
						  ChatMessage.create({user: game.user._id, speaker: ChatMessage.getSpeaker({token: actor}), content: result});
					  }
					}
			  },
				damage: {
					label: "Necrotic", callback: async (html) => {
						let target = canvas.tokens.get(args[0].targets[0]._id);
					  let damageRoll = new Roll('4d10').roll();
					  new MidiQOL.DamageOnlyWorkflow(actor, token, damageRoll.total, "necrotic", [target], damageRoll, {flavor: "Spiked Chain", damageList: args[0].damageList, itemCardId: args[0].itemCardId});
					}
				}
			}
		}).render(true);
	}
})();
