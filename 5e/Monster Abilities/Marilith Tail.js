// Marilith Tail | MidiQOL onUse/ItemMacro
(async()=>{
	if (args[0].targets.length === 0 || args[0].hitTargets.length === 0) return;
	let attacker = canvas.tokens.get(args[0].tokenId);
	let target = canvas.tokens.get(args[0].hitTargets[0]._id);
	let size = ["tiny", "sm", "med"].some(size => (target.actor.data.data.traits.size || "").includes(size));
  if (size && !game.cub.hasCondition("Grappled", target)) {
    await game.cub.addCondition(["Grappled", "Restrained"], target)
    let result = `${target.name} is grappled and restrained by the marilith's tail!`;
	  ChatMessage.create({user: game.user._id, speaker: ChatMessage.getSpeaker({token: attacker}), content: result});
  }
})();
