// This is 2 separate macros for the Chain Devil's Chain attack.

// Macro 1:
// Chain MidiQOL onUse/ItemMacro
(async()=>{
	if (args[0].targets.length === 0 || args[0].hitTargets.length === 0) return;
	let attacker = canvas.tokens.get(args[0].tokenId);
	let target = canvas.tokens.get(args[0].hitTargets[0]._id);
  if (!game.cub.hasCondition(["Grappled", "Restrained"], target)) {
    await game.cub.addCondition(["Grappled", "Restrained"], target)
    let result = `${target.name} is grappled and restrained by the chain!`;
	  ChatMessage.create({user: game.user._id, speaker: ChatMessage.getSpeaker({token: attacker}), content: result});
  }
})();

// Macro 2:
// DAE Usage: macro.execute CUSTOM "Kyton Chain" @token @item 2d6 piercing
// Set macro repeat to start of each turn.
if (args[0] === "each") {
	const lastArg = args[args.length-1];
	let attacker = canvas.tokens.get(args[1]);
	let target = canvas.tokens.get(lastArg.tokenId);
	const item = args[2];
  const damageFormula = args[3];
  const damageType = args[4];
  let damageRoll = new Roll(damageFormula).roll();
  MidiQOL.applyTokenDamage([{damage: damageRoll.total, type: damageType}], damageRoll.results, new Set([target]), `${item.name}`, new Set());
  damageRoll.toMessage({flavor: `${item.name} - Damage Roll (<span style="text-transform: capitalize;">${damageType}</span>)`, user: game.user._id, speaker: ChatMessage.getSpeaker({token: attacker})});
}
