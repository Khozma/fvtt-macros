// Archer's Eye | MidiQOL onUse/ItemMacro
// Archer NPC ability. Set target to self.
if (args[0].targets.length === 0) return ui.notifications.warn(`No target was selected.`);
(async()=>{
	let actorD = args[0].actor;
	let target = canvas.tokens.get(args[0].targets[0]._id);
	let type = "";

	async function createEffect(type) {
		const effectLabel = "Archer's Eye";
		const effectIcon = ""; // Effect image can be whatever
    const effectOrigin = `Actor.${args[0].actor._id}.OwnedItem.${args[0].item._id}`;

		let effect = target.actor.effects.find(i => i.data.label === effectLabel);
		if (effect === null) {
			let currentTurn = null;
			if (game.combat && game.combat.started) currentTurn = game.combat.turn;
			if (type === "Attack") {
				let effectData = {
			    label: effectLabel,
			    icon: effectIcon,
			    origin: effectOrigin,
		      "flags": {
		        "dae": {
		          "stackable": false,
		          "specialDuration": "1Attack",
		          "transfer": false
		        }
		      },
		      "changes": [
		        {
		          "key": "data.bonuses.rwak.attack",
		          "mode": 0,
		          "value": "1d10",
		          "priority": 60
		        }
		      ],
		      "duration": {
		        "seconds": null,
		        "startTime": null,
		        "rounds": null,
		        "turns": 1,
		        "startRound": null,
		        "startTurn": currentTurn
		      }
				};
				await target.actor.createEmbeddedEntity("ActiveEffect", effectData);
			}
			else if (type === "Damage") {
				let effectData = {
			    label: effectLabel,
			    icon: effectIcon,
			    origin: effectOrigin,
		      "flags": {
		        "dae": {
		          "stackable": false,
		          "specialDuration": "1Hit",
		          "transfer": false
		        }
		      },
		      "changes": [
		        {
		          "key": "data.bonuses.rwak.damage",
		          "mode": 0,
		          "value": "1d10",
		          "priority": 60
		        }
		      ],
		      "duration": {
		        "seconds": null,
		        "startTime": null,
		        "rounds": null,
		        "turns": 1,
		        "startRound": null,
		        "startTurn": currentTurn
		      }
				};
				await target.actor.createEmbeddedEntity("ActiveEffect", effectData);
			}
		}
		else {
			return;
		}
	}

	if (args[0].targets.length === 1) {
		let dialogContent = `<div style="text-align:center;"><p>Choose the type of roll to benefit:</p></div>`;
		new Dialog({
			title: "Archer's Eye",
			content: dialogContent,
			buttons: {
				attack: {
					label: "Attack", callback: async (html) => {
						await createEffect("Attack");
					}
			  },
				damage: {
					label: "Damage", callback: async (html) => {
						await createEffect("Damage");
					}
				}
			}
		}).render(true);
	}
})();
