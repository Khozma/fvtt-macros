// Howler Rending Bite | MidiQOL onUse/ItemMacro
// Does an extra 4d10 psychic damage on a frightened target.
if (args[0].targets.length === 0 || args[0].hitTargets.length === 0) return;
let attacker = canvas.tokens.get(args[0].tokenId);
let target = canvas.tokens.get(args[0].hitTargets[0]._id);
if (game.cub.hasCondition("Frightened", target)) {
  let damageRoll = new Roll('4d10').roll();
	new MidiQOL.DamageOnlyWorkflow(actor, token, damageRoll.total, "psychic", [target], damageRoll, {flavor: "Rending Bite", damageList: args[0].damageList, itemCardId: args[0].itemCardId});
}
