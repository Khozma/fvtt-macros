// Air Elemental Whirlwind | MidiQOL onUse/ItemMacro
(async()=>{
	if (args[0].targets.length === 0 || args[0].hitTargets.length === 0 || args[0].failedSaves.length === 0) return;
	if (args[0].failedSaves.length > 0) {
		let attacker = canvas.tokens.get(args[0].tokenId);
		let failedSaves = args[0].failedSaves;
		for (let failedTarget of failedSaves) {
			let target = canvas.tokens.get(failedTarget._id);
			let object = (target.actor.data.data.details.type || "").toLowerCase().includes(["object"]) || (target.actor.data.type !== "character" && target.actor.data.type !== "npc");
			if (!object) {
				await game.cub.addCondition("Prone", target, {warn: false});
			  let content = `${target.name} is flung up 20 feet away from the elemental in a random direction and knocked prone!`;
			  ChatMessage.create({user: game.user._id, speaker: ChatMessage.getSpeaker({token: attacker}), content});
			}
		}
	}
})();
