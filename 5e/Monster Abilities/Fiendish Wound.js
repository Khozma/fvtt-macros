// Fiendish Wound
// Damages affected target on each of their turns, multiplying the number of damage dice by the number of times this effect has been applied.
// Usage (e.g): Macro => Execute "Fiendish Wound" @target @token "slashing" 2 d4
const lastArg = args[args.length-1];
let target = (lastArg.tokenId) ? canvas.tokens.get(lastArg.tokenId) : canvas.tokens.get(args[1]);
const damageType = args[3].toLowerCase();

async function damage(target, wound) {
  let numDice = parseInt(args[4]);
  let diceType = args[5];
  let diceRolls = '';
  numDice = numDice * wound;
  let damageRoll = new Roll(`${numDice}${diceType}`).roll();
  for (let diceRoll of damageRoll.terms[0].results) diceRolls += `<li class="roll die ${diceType} min">${diceRoll.result}</li>`;
  let content = `<div class="dnd5e chat-card item-card midi-qol-item-card" data-actor-id="" data-item-id="${lastArg.efData.flags.dae.itemData._id}" data-spell-level="">
                  <header class="card-header flexrow">
                    <img src="${lastArg.efData.icon}" title="${lastArg.efData.label}" width="36" height="36" />
                    <h3 class="item-name">${lastArg.efData.label}</h3>
                  </header>

                  <div class="card-content">${lastArg.efData.flags.dae.itemData.data.description.value}</div>

                  <div class="card-buttons">
                    <div>
                      <div class="flexrow 1">
                        <div class="midi-qol-attack-roll">
                          <div class="end-midi-qol-attack-roll"></div>
                        </div>
                      </div>
                      <div class="flexrow 1">
                        <div class="midi-qol-damage-roll" style="text-align:center;text-transform:capitalize;">
                          (${damageType})
                          <div class="dice-roll">
                            <div class="dice-result">
                              <div class="dice-formula">${damageRoll.formula}</div>
                              <div class="dice-tooltip">
                                <div class="dice">
                                  <header class="part-header flexrow">
                                    <span class="part-formula">${numDice}${diceType}</span>
                                    <span class="part-total">${numDice}</span>
                                  </header>
                                  <ol class="dice-rolls">${diceRolls}</ol>
                                </div>
                              </div>
                              <h4 class="dice-total">${damageRoll.total}</h4>
                            </div>
                          </div>
                          <div class="end-midi-qol-damage-roll"></div>
                        </div>
                      </div>
                      <div class="flexrow 1">
                        <div class="midi-qol-other-roll">
                          <div class="end-midi-qol-other-roll">
                        </div>
                        <div class="midi-qol-hits-display">
                          <div class="end-midi-qol-hits-display"></div>
                        </div>
                      </div>
                    </div>
                    <div class="midi-qol-saves-display">
                      <div class="end-midi-qol-saves-display"></div>
                    </div>
                  </div>

                  <footer class="card-footer" style="padding: 0px;"></footer>
                </div>`;
  ChatMessage.create({user: game.user._id, speaker: ChatMessage.getSpeaker({token: target}), content});
  MidiQOL.applyTokenDamage([{damage: damageRoll.total, type: damageType}], damageRoll.results, new Set([target]), lastArg.efData.flags.dae.itemData.name, new Set());
}

async function removeEffect(target) {
  await target.actor.deleteEmbeddedEntity("ActiveEffect", target.actor.effects.filter(i => i.data.label.includes([lastArg.efData.label])).map(i => i.data._id));
}

if (args[0] === "on") {
  let flag = await DAE.getFlag(target, "fiendish_wound");
  if (!flag) {
    DAE.setFlag(target, "fiendish_wound", {counter: 1})
  }
  else {
    let wound = flag.counter + 1;
    DAE.setFlag(target, "fiendish_wound", {counter: wound})
  }
}

if (args[0] === "each") {
  let immuneDI = [damageType].some(di => (target.actor.data.data.traits.di.value || "").includes(di));
  if (target.actor.data.data.attributes.hp.value <= 0 || immuneDI) removeEffect(target);
  else {
    let wound;
    let flag = await DAE.getFlag(target, "fiendish_wound");
    if (!flag) wound = 1;
    else wound = flag.counter;
    damage(target, wound);
  }
}

if (args[0] === "off") {
  DAE.unsetFlag(target, "fiendish_wound");
}
