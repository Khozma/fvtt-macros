// Dybbuk Tendrils | MidiQOL OnUse ItemMacro
// Only life drain ability I've found on a monster (so far) that couldn't be worked into the main one.
if (args[0].targets.length === 0 || args[0].hitTargets.length === 0) return;
let attacker = canvas.tokens.get(args[0].tokenId);
let target = canvas.tokens.get(args[0].hitTargets[0]._id);
let object = (target.actor.data.data.details.type || "").toLowerCase().includes(["object"]) || (target.actor.data.type !== "character" && target.actor.data.type !== "npc");
let immuneCI = ["life drain"].some(ci => (target.actor.data.data.traits.ci.custom || "").toLowerCase().includes(ci)) || ["exhaustion"].some(ci => (target.actor.data.data.traits.ci.value || "").includes(ci));

if (!object && !immuneCI) {
	let damageRoll = new Roll('1d6').roll();
	new MidiQOL.DamageOnlyWorkflow(actor, token, damageRoll.total, "midi-none", [target], damageRoll, {flavor: "Life Drain", damageList: args[0].damageList, itemCardId: args[0].itemCardId});

	// Effect label, icon, and origin
	const effectLabel = "Life Drain";
	const effectIcon = "systems/dnd5e/icons/skills/shadow_18.jpg";
	const effectOrigin = `Actor.${args[0].actor._id}.OwnedItem.${args[0].item._id}`;

	// Update the effect if already present
	let effect = target.actor.effects.find(i => i.data.label === effectLabel);
	if (effect !== null) {
		let changes = effect.data.changes;
		let effectHpMax = (Number(changes[0].value)) - damageRoll.total;
	  changes[0].value = effectHpMax;
	  console.log(`Life Drain | Updating ActiveEffect on ${target.name}:`, effect, changes);
	  await effect.update({changes});
	}
	else { // Otherwise, create the effect
		const effectData = {
	    label: effectLabel,
	    icon: effectIcon,
	    origin: effectOrigin,
	    changes: [{
	      "key": "data.attributes.hp.max",
	      "mode": 2,
	      "value": -damageRoll.total,
	      "priority": "20"
	    }]
		};
		console.log(`Life Drain | Creating ActiveEffect on ${target.name}:`, effectData);
		await target.actor.createEmbeddedEntity("ActiveEffect", effectData);
	}
}
