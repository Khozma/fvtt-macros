// DEPRECATED.
// Superseded by MidiQOL 0.3.63's concentration feature, and mostly even by my other, much better written Concentration OnUse macro before it.
// Probably best not to use anymore. Only leaving up right now since I haven't updated all my old macros to remove references to this one yet.
// -----
// GM macro. Updates Concentrating condition on caster to have a duration of the spell effect and removes it if the spell effect ends.
// The spell must have an effect of the same name applied to the caster or the target(s) to be able to update the duration.
// Concentration auto-removal can be disabled with a 4th arg "onlyOn", for spells that can end at different times on each target.
// DAE Usage: Macro => Execute "Concentrating" @actor @target @item
(async ()=>{
	async function wait(ms) {
	  return new Promise(resolve => {
	    setTimeout(resolve, ms);
	  });
	}

  if (args[0] === "on") {
  	if (args[2] === null || args[2] === undefined || args[3] === null || args[3] === undefined) return;
    const caster = await canvas.tokens.placeables.find(t => t.name === args[1].name);
  	const target = await canvas.tokens.get(args[2]);
    await wait(1000);
    
    const concentrationEffect = caster.actor.effects.find(i => i.data.label === "Concentrating");
    const spellEffect = target.actor.effects.find(i => i.data.label === args[3].name);
    await wait(250);
    if (concentrationEffect !== null && spellEffect !== null && spellEffect !== undefined) {
      let updated = false;
      let flag = null;
      flag = (concentrationEffect.data.flags.world || undefined);
      if (flag) updated = (concentrationEffect.data.flags.world.concentrating || false);

      if (updated) {
        console.log(`Concentrating | ${args[1].name}: effect already updated`, concentrationEffect.data.flags.world);
        return;
      }
      else {
        let updatedEffect = duplicate(concentrationEffect);
        let updatedEffectD = updatedEffect.data;
        updatedEffectD.disabled = spellEffect.data.disabled;
        updatedEffectD.duration = spellEffect.data.duration;
        updatedEffectD.flags.dae = spellEffect.data.flags.dae;
        updatedEffectD.flags.world = {"concentrating": true};
        updatedEffectD.origin = `Actor.${args[1]._id}.OwnedItem.${args[3]._id}`;
        await caster.actor.updateEmbeddedEntity("ActiveEffect", updatedEffectD);
        console.log(`Concentrating | ${args[1].name}: updated effect`, updatedEffectD);
      }
    }
  }

  if (args[0] === "off") {
  	if (args[4] === "onlyOn") return;
    let target = await canvas.tokens.placeables.find(t => t.name === args[1].name);
    if (game.cub.hasCondition("Concentrating", target)) {
      await game.cub.removeCondition("Concentrating", target, {warn: false});
      console.log(`Concentrating | ${args[1].name}: ended effect`);
    }
  }
})();
