// Concentrating OnUse | MidiQOL OnUse Macro
// Updates Concentrating condition effect duration to match the spell's duration.
// Not needed if using MidiQOL 0.3.63's own concentration feature, but can still be used without it.
(async ()=>{
	const cToken = await canvas.tokens.get(args[0].tokenId);
	const cActor = (cToken) ? cToken.actor : game.actors.get(args[0].actor._id);
	const item = args[0].item;

	async function wait(ms) {
	  return new Promise(resolve => {
	    setTimeout(resolve, ms);
	  });
	}

	function getSeconds(i) {
		let duration = i.data.duration;
		if (duration.units === "inst") return 1;
		else if (duration.units === "minute") return 60 * (Number(duration.value) || 1);
		else if (duration.units === "hour") return 60 * 60 * (Number(duration.value) || 1);
		else if (duration.units === "day") return 60 * 60 * 24 * (Number(duration.value) || 1);
		else if (duration.units === "round" || duration.units === "turn") return 6 * (Number(duration.value) || 1);
		else if (duration.units === "perm" || duration.units === "spec") return 6;
		else return 6;
	}

  await wait(1000);
  const efConcentration = cActor.effects.find(i => i.data?.label === "Concentrating");
	if (efConcentration && efConcentration.data?.flags?.world?.concentrating !== "updated") {
		let efUpdate = duplicate(efConcentration);
		let efUpdateData = efUpdate.data;
		let efSeconds = getSeconds(item);
		let useRounds = (game.combat?.started && efSeconds <= 60 && (item.data.duration.units === "round" || item.data.duration.units === "turn"));
		efUpdateData.origin = `Actor.${args[0].actor._id}.OwnedItem.${args[0].item._id}`;
		efUpdateData.duration.seconds = efSeconds;
		efUpdateData.duration.startTime = game.time.worldTime;
		efUpdateData.duration.rounds = (useRounds) ? efSeconds / 6 : null;
		efUpdateData.duration.turns = (useRounds) ? efSeconds / 6 : null;
		efUpdateData.duration.startRound = (useRounds) ? game.combat?.round : null;
		efUpdateData.duration.startTurn = (useRounds) ? game.combat?.turn : null;
		efUpdateData.flags.world = {"concentrating": "updated"};
		await cActor.updateEmbeddedEntity("ActiveEffect", efUpdateData);
		console.log(`Concentrating OnUse | ${cActor.name} - effect updated:`, efUpdateData);
	}
	else if (efConcentration && efConcentration.data?.flags?.world?.concentrating === "updated") console.log(`Concentrating OnUse | ${cActor.name} - effect already updated:`, efConcentration.data);
	else return;
})();
