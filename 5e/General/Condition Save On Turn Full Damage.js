// Condition Save On Turn Damage
// Applies a condition when DAE effect is applied, triggers a save on the creature's turns, deals full damage, and removes the effect on a success.
// The last 3 args below can be any DC, save type, or condition type. Set macro repeat to start or end of each turn.
// Usage (e.g): Macro => Execute "Condition Save On Turn Damage" @token @attributes.spelldc "wis" "Frightened" "psychic" 2d4
//                           or: "Condition Save On Turn Damage" @token @abilities.str.dc "con" "Poisoned" "poison" 1d10
//                               (etc)
const lastArg = args[args.length-1];
let target = canvas.tokens.get(lastArg.tokenId);
const item = lastArg.efData.flags.dae.itemData;
const saveDC = (Number(item.data.save.dc) || parseInt(args[2]));
const condition = args[4].charAt(0).toUpperCase();
const damageType = args[5].toLowerCase();
let immuneCI;
let immuneDI;
if (target) {
  immuneCI = [condition.toLowerCase()].some(ci => (target.actor.data.data.traits.ci.value || "").includes(ci));
  immuneDI = [damageType.toLowerCase()].some(di => (target.actor.data.data.traits.di.value || "").includes(di));
}

async function save(target, saveType, damageDice) {
  let damageRoll = new Roll(`${damageDice}`).roll();
  let saveRoll = (await target.actor.rollAbilitySave(saveType, {fastForward: true, chatMessage: true}));
  if (saveRoll === null) return;

  if (saveRoll.total >= saveDC) {
    if (game.cub.hasCondition(condition, target, {warn: false})) await game.cub.removeCondition(condition, target, {warn: false});
    let results_html = `<div class="dnd5e chat-card item-card midi-qol-item-card" data-actor-id="" data-item-id="${item._id}" data-spell-level="">
                            <header class="card-header flexrow">
                              <img src="${lastArg.efData.icon}" title="${lastArg.efData.label}" width="36" height="36" />
                              <h3 class="item-name">${lastArg.efData.label}</h3>
                            </header>

                            <div class="card-content">${item.data.description.value}</div>

                            <div class="card-buttons">
                              <div>
                                <div class="flexrow 1">
                                  <div class="midi-qol-attack-roll">
                                    <div class="end-midi-qol-attack-roll"></div>
                                  </div>
                                </div>
                                <div class="flexrow 1">
                                  <div class="midi-qol-damage-roll" style="text-align:center;text-transform:capitalize;">
                                    (${damageType})
                                    <div class="dice-roll">
                                      <div class="dice-result">
                                        <div class="dice-formula">${damageRoll.formula}</div>
                                        <div class="dice-tooltip">${damageRoll.results}</div>
                                        <h4 class="dice-total">${damageRoll.total}</h4>
                                      </div>
                                    </div>
                                    <div class="end-midi-qol-damage-roll"></div>
                                  </div>
                                </div>
                                <div class="flexrow 1">
                                  <div class="midi-qol-other-roll">
                                    <div class="end-midi-qol-other-roll">
                                  </div>
                                  <div class="midi-qol-hits-display">
                                    <div class="end-midi-qol-hits-display"></div>
                                  </div>
                                </div>
                              </div>
                              <div class="midi-qol-saves-display">
                                <div data-item-id="${item._id}">
                                  <div class="midi-qol-nobox midi-qol-bigger-text">${lastArg.efData.label} DC ${saveDC} ${CONFIG.DND5E.abilities[saveType]} Saving Throw:</div>
                                  <div>
                                    <div class="midi-qol-nobox">
                                      <div class="midi-qol-bigger-text">
                                        <span>Full damage on save</span>
                                      </div>
                                      <div class="midi-qol-flex-container">
                                        <div class="midi-qol-target-npc midi-qol-target-name" id="${target.id}">${target.name}</div> <div>saves with ${saveRoll.total}</div> <div><img src="${target.data.img}" width="30" height="30" style="border:0px" /></div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="end-midi-qol-saves-display"></div>
                              </div>
                            </div>

                          <footer class="card-footer" style="padding: 0px;"></footer>
                        </div>`;
    ChatMessage.create({user: game.user._id, speaker: ChatMessage.getSpeaker({token: target}), content: results_html});
    MidiQOL.applyTokenDamage([{damage: damageRoll.total, type: damageType}], damageRoll.results, new Set([target]), item.name, new Set());
    removeEffect(target);
  }
  else {
    let results_html = `<div class="dnd5e chat-card item-card midi-qol-item-card" data-actor-id="" data-item-id="${item._id}" data-spell-level="">
                          <header class="card-header flexrow">
                            <img src="${lastArg.efData.icon}" title="${lastArg.efData.label}" width="36" height="36" />
                            <h3 class="item-name">${lastArg.efData.label}</h3>
                          </header>

                          <div class="card-content">${item.data.description.value}</div>

                          <div class="card-buttons">
                            <div>
                              <div class="flexrow 1">
                                <div class="midi-qol-attack-roll">
                                  <div class="end-midi-qol-attack-roll"></div>
                                </div>
                              </div>
                              <div class="flexrow 1">
                                <div class="midi-qol-damage-roll" style="text-align:center;text-transform:capitalize;">
                                  (${damageType})
                                  <div class="dice-roll">
                                    <div class="dice-result">
                                      <div class="dice-formula">${damageRoll.formula}</div>
                                      <div class="dice-tooltip">${damageRoll.results}</div>
                                      <h4 class="dice-total">${damageRoll.total}</h4>
                                    </div>
                                  </div>
                                  <div class="end-midi-qol-damage-roll"></div>
                                </div>
                              </div>
                              <div class="flexrow 1">
                                <div class="midi-qol-other-roll">
                                  <div class="end-midi-qol-other-roll">
                                </div>
                                <div class="midi-qol-hits-display">
                                  <div class="end-midi-qol-hits-display"></div>
                                </div>
                              </div>
                            </div>
                            <div class="midi-qol-saves-display">
                              <div data-item-id="${item._id}">
                                <div class="midi-qol-nobox midi-qol-bigger-text">${lastArg.efData.label} DC ${saveDC} ${CONFIG.DND5E.abilities[saveType]} Saving Throw:</div>
                                <div>
                                  <div class="midi-qol-nobox">
                                    <div class="midi-qol-bigger-text">
                                      <span>Full damage on save</span>
                                    </div>
                                    <div class="midi-qol-flex-container">
                                      <div class="midi-qol-target-npc midi-qol-target-name" id="${target.id}">${target.name}</div> <div>fails with ${saveRoll.total}</div> <div><img src="${target.data.img}" width="30" height="30" style="border:0px" /></div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="end-midi-qol-saves-display"></div>
                            </div>
                          </div>

                          <footer class="card-footer" style="padding: 0px;"></footer>
                        </div>`;
    ChatMessage.create({user: game.user._id, speaker: ChatMessage.getSpeaker({token: target}), content: results_html});
    MidiQOL.applyTokenDamage([{damage: damageRoll.total, type: damageType}], damageRoll.results, new Set([target]), item.name, new Set());
  }
}

async function removeEffect(target) {
  await target.actor.deleteEmbeddedEntity("ActiveEffect", lastArg.effectId);
}

if (args[0] === "on") {
  if (!game.cub.hasCondition(condition, target, {warn: false}) && !immuneCI) game.cub.addCondition(condition, target, {warn: false});
}

if (args[0] === "each") {
  if (target.actor.data.data.attributes.hp.value <= 0 || immuneCI || immuneDI) {
    if (game.cub.hasCondition(condition, target, {warn: false})) await game.cub.removeCondition(condition, target, {warn: false});
    removeEffect(target);
  }
  else {
    save(target, args[3], args[6]);
  }
}
