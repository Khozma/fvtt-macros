// Delete This Token | DAE Macro/Requires MidiQOL
// Usage: Macro => Execute "Delete This Token" @token
// Put on a feature with target Self. Deletes the token as well as the item card sent by the feature to the chat.
if (args[0] === "on") {
	const lastArg = args[args.length-1];
	// Delete message
	let actorId = lastArg.actorId;
	let itemId = lastArg.efData.flags.dae.itemData._id;
	let itemName = lastArg.efData.flags.dae.itemData.name;
	let msgHistory = await game.messages.entities.map(i => ({_id: i.data?._id, user: i.data.user, flavor: i.data.flavor, actorId: i.data?.flags["midi-qol"]?.actor, itemId: i.data?.flags["midi-qol"]?.itemId}))?.filter(i => i.actorId === actorId && i.itemId === itemId && i.flavor === itemName);
	if (msgHistory.length > 0) {
		let lastMsg = msgHistory[msgHistory.length-1];
		if (lastMsg) await ui.chat.deleteMessage(lastMsg._id, false);	
	}
	// Delete token
	actor = game.actors.get(lastArg.actorId);
	let summon = canvas.tokens.get(args[1]);
	if (!summon) summon = await canvas.tokens.placeables.find(i => i.name === actor.data.name);
	if (summon) await canvas.tokens.deleteMany(summon.data._id);
}
