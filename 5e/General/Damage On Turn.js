// Damage On Turn
// Damages a creature on its turns.
// Usage (e.g): Macro => Execute "Damage On Turn" @target "fire" 1d10
const lastArg = args[args.length-1];
let target = (lastArg.tokenId) ? canvas.tokens.get(lastArg.tokenId) : canvas.tokens.get(args[1]);
const damageType = args[2].toLowerCase();
let immuneDI;
if (target) immuneDI = [damageType].some(di => (target.actor.data.data.traits.di.value || "").includes(di));

async function damage(target, formula) {
  let damageRoll = new Roll(`${formula}`).roll();
  let content = `<div class="dnd5e chat-card item-card midi-qol-item-card" data-actor-id="" data-item-id="${lastArg.efData.flags.dae.itemData._id}" data-spell-level="">
                  <header class="card-header flexrow">
                    <img src="${lastArg.efData.icon}" title="${lastArg.efData.label}" width="36" height="36" />
                    <h3 class="item-name">${lastArg.efData.label}</h3>
                  </header>

                  <div class="card-content">${lastArg.efData.flags.dae.itemData.data.description.value}</div>

                    <div class="card-buttons">
                      <div>
                        <div class="flexrow 1">
                          <div class="midi-qol-attack-roll">
                            <div class="end-midi-qol-attack-roll"></div>
                          </div>
                        </div>
                        <div class="flexrow 1">
                          <div class="midi-qol-damage-roll" style="text-align:center;text-transform:capitalize;">
                            (${damageType})
                            <div class="dice-roll">
                              <div class="dice-result">
                                <div class="dice-formula">${damageRoll.formula}</div>
                                <div class="dice-tooltip">${damageRoll.results}</div>
                                <h4 class="dice-total">${damageRoll.total}</h4>
                              </div>
                            </div>
                            <div class="end-midi-qol-damage-roll"></div>
                          </div>
                        </div>
                        <div class="flexrow 1">
                          <div class="midi-qol-other-roll">
                            <div class="end-midi-qol-other-roll">
                          </div>
                          <div class="midi-qol-hits-display">
                            <div class="end-midi-qol-hits-display"></div>
                          </div>
                        </div>
                      </div>
                      <div class="midi-qol-saves-display">
                        <div class="end-midi-qol-saves-display"></div>
                      </div>
                    </div>

                  <footer class="card-footer" style="padding: 0px;"></footer>
                </div>`;
  ChatMessage.create({user: game.user._id, speaker: ChatMessage.getSpeaker({token: target}), content});
  MidiQOL.applyTokenDamage([{damage: damageRoll.total, type: damageType}], damageRoll.results, new Set([target]), lastArg.efData.flags.dae.itemData.name, new Set());
}

async function removeEffect(target) {
  await target.actor.deleteEmbeddedEntity("ActiveEffect", lastArg.effectId);
}

if (args[0] === "each") {
  if (target.actor.data.data.attributes.hp.value <= 0 || immuneDI) removeEffect(target);
  else damage(target, args[3]);
}
