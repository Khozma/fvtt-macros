// Sneak Attack | MidiQOL DamageBonus Macro
// Add to a feature as a transfer effect that sets flags.dnd5e.DamageBonusMacro to the macro's name.
// Automatically applies sneak attack damage if the appropriate conditions are met.
if (args[0].hitTargets.length === 0) return;
if (args[0].tag === "DamageBonus") {
  token = canvas.tokens.get(args[0].tokenId);
  let tToken = canvas.tokens.get(args[0].hitTargets[0]._id);
  if (!token || !tToken) return {};
  let distance = 9.5;
  let nearbyAlly = (await canvas.tokens.placeables.filter(target => (canvas.grid.measureDistance(tToken.center, target.center) <= distance && token.id != target.id && token.data.disposition === target.data.disposition && !game.cub.hasCondition("Incapacitated", target, {warn: false})))?.length > 0);
  let advantage = args[0].advantage ?? false;
  let disadvantage = args[0].disadvantage ?? false;
  let alreadyUsed = token.actor.effects?.entries?.find(ef => ef?.data.label === `Sneak Attack (already used this turn)`) ? true : false;
  if (!["mwak","rwak"].includes(args[0].item.data.actionType) || (!["rwak"].includes(args[0].item.data.actionType) && !args[0].item.data.properties.fin) || (!nearbyAlly && !advantage) || disadvantage || alreadyUsed) return {};
  let itemD = args[0].actor.items?.find(i => i?.type === "feat" && i?.name === "Sneak Attack");
  const efData = {
    label: `Sneak Attack (already used this turn)`,
    icon: "",
    transfer: false,
    tint: "",
    origin: itemD ? `Actor.${args[0].actor._id}.OwnedItem.${itemD._id}` : '',
    flags: {
      dae: {
        stackable: false,
        macroRepeat: "none",
        specialDuration: [
          "turnEnd"
        ],
        transfer: false
      }
    },
    changes: [],
    duration: {
      seconds: 1,
      startTime: game.time.worldTime,
      rounds: null,
      turns: 1,
      startRound: null,
      startTurn: game.combat?.turn
    }
  }
  await token.actor.createEmbeddedEntity("ActiveEffect", efData);
  let level = args[0].actor.items?.find(i => i?.name === `Rogue`)?.data.levels || 7; // Use Rogue level, and if not found, use level 7 damage as on the Assassin/Master Thief monster statblocks
  let isCritical = args[0].isCritical || args[0].damageRoll.terms[0].options?.critical;
  let numDice = Math.ceil(level / 2);
  if (isCritical) numDice = numDice * 2;
  let damageType = args[0].item.data.damage.parts[0][1];
  damageType = damageType.charAt(0).toUpperCase() + damageType.slice(1);
  return {damageRoll: `${numDice}d6[${damageType}]`, flavor: `Sneak Attack`}
}
