// Metamagic - Twinned Spell | MidiQOL onUse/ItemMacro
// Opens a prompt to choose the spell slot used and subtracts points from an attribute labelled "Sorcery Points".
let actorId = args[0].actor._id;
let tokenId = args[0].tokenId;
if (!actorId) return;
actor = (tokenId) ? canvas.tokens.get(tokenId).actor : game.actors.get(actorId);
let item = args[0].item;

let spellSlots = '';
for (let slot in actor.data.data.spells) {
  if (actor.data.data.spells[slot].max > 0) {
    let slotValue = slot.charAt(slot.length-1);
    spellSlots += `<option value="${slotValue}">Level ${slotValue}</option>`;
  }
}

let resource = '';
let sorceryPoints;
if (actor.data.data.resources.primary.label === "Sorcery Points") {
  resource = 'primary';
  sorceryPoints = actor.data.data.resources.primary.value;
}
if (actor.data.data.resources.secondary.label === "Sorcery Points") {
  resource = 'secondary';
  sorceryPoints = actor.data.data.resources.secondary.value;
}
if (actor.data.data.resources.tertiary.label === "Sorcery Points") {
  resource = 'tertiary';
  sorceryPoints = actor.data.data.resources.tertiary.value;
}

async function spellDialog(spellSlots) {
  let content = `<div>
                  <form class="flexcol">
                    <div class="form-group">
                      <div style="white-space: pre-wrap;">Choose the spell slot used: </div>
                      <select id="spellSlot">
                        <option value="1">Cantrip</option>
                        ${spellSlots}
                      </select>
                    </div>
                  </form>
                </div>`;
  new Dialog({
    title: item.name,
    content,
    buttons: {
      done: {
        icon: '<i class="fas fa-check"></i>',
        label: '',
        callback: async (html) => {
          let value = parseInt(html.find('#spellSlot').val());
          sorceryPoints = sorceryPoints - value;
          if (sorceryPoints < 0) {
            ui.notifications.warn(`You don't have enough enough sorcery points!`);
            return;
          }
          let resourceValue = `data.resources.${resource}.value`;
          let actorData = duplicate(actor._data);
          actorData.data.resources[`${resource}`].value = sorceryPoints;
          await actor.update(actorData);
          ui.notifications.info(`<strong>${actor.name}</strong> used <strong>${value}</strong> Sorcery Points with <strong>${sorceryPoints}/${actorData.data.resources[`${resource}`].max}</strong> Sorcery Points now remaining!`);
        }
      }
    }     
  }).render(true);
}
spellDialog(spellSlots);
